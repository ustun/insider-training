// #!/bin/bash
//     /Applications/Google\ Chrome\ Canary.app/Contents/MacOS/Google\ Chrome\ Canary  \
// --headless --disable-gpu --screenshot --window-size=1280,1696 \
// http://www.useinsider.com/

// http://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/

const { Builder, By, until, promise } = require("selenium-webdriver");
const fs = require("fs");
promise.USE_PROMISE_MANAGER = false;

let driver;

async function saveScreenshot(dosyaIsmi) {
    var screenshotData = await driver.takeScreenshot();

    fs.writeFileSync(dosyaIsmi + ".png", screenshotData, "base64");
}

class Page {
    constructor(name, path) {
        this.name = name;
    }

    takeScreenshot() {
        saveScreenshot(name + Date.now());
    }
}

class Login extends Page {}

async function main() {
    driver = await new Builder().forBrowser("chrome").build();
    await driver.get("https://www.google.com/ncr");

    await driver.findElement(By.name("q")).sendKeys("webdriver");

    var x = await driver.findElement(By.id("pocs2")).getText();

    var isim = "Ahmet";

    function titleAl(isim) {
        return document.title;
    }

    var result = await driver.executeScript(titleAl);
    saveScreenshot("a");
    console.log(result);

    // #sticky-header-control > header > div > div > div > nav > ul > li:nth-child(1) > a

    await driver.findElement(By.name("btnG")).click();

    await driver.wait(until.titleIs("webdriver - Google Search"), 1000);

    saveScreenshot("b");

    // await driver.quit();
}

main();
