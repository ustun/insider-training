import React, { Component } from "react";
import { TodoApp } from "./TodoApp";

class TodoComponent extends Component {
    handleChange = event => {
        this.props.onTodoToggle(this.props.todo.description);
    };

    render() {
        return (
            <li
                data-id={1497185673610}
                className={this.props.todo.completed ? "completed" : ""}
            >
                <div className="view">
                    <input
                        className="toggle"
                        type="checkbox"
                        onChange={this.handleChange}
                        checked={this.props.todo.completed}
                    />
                    <label>{this.props.todo.description}</label>
                    <button className="destroy" />
                </div>
            </li>
        );
    }
}
export default class TodoAppComponent extends Component {
    constructor() {
        super();
        var model = new TodoApp();
        model.setGeciciTodo("utu yap");
        model.addNewTodo();
        model.setGeciciTodo("yemek yap");

        this.state = { model: model };
    }

    handleChange = event => {
        var newTempTodo = event.target.value;

        this.state.model.setGeciciTodo(newTempTodo);

        this.setState({ model: this.state.model });
    };

    handleSubmit = event => {
        event.preventDefault();
        this.state.model.addNewTodo();

        this.setState({ model: this.state.model });
    };

    handleTodoToggle = description => {
        this.state.model.toggleTodo(description);

        this.setState({ model: this.state.model });
    };

    render() {
        return (
            <section className="todoapp">
                <header className="header">
                    <h1>todos</h1>
                    <form onSubmit={this.handleSubmit}>
                        <input
                            className="new-todo"
                            placeholder="What needs to be done?"
                            autofocus
                            value={this.state.model.geciciTodo}
                            onChange={this.handleChange}
                        />
                    </form>
                    <div>{this.state.model.geciciTodo}</div>
                </header>
                <section className="main" style={{ display: "block" }}>
                    <input className="toggle-all" type="checkbox" />
                    <label htmlFor="toggle-all">Mark all as complete</label>
                    <ul className="todo-list">

                        {this.state.model
                            .getVisibleTodos()
                            .map(todo => (
                                <TodoComponent
                                    key={todo.description}
                                    todo={todo}
                                    onTodoToggle={this.handleTodoToggle}
                                />
                            ))}

                    </ul>
                </section>
                <footer className="footer" style={{ display: "block" }}>
                    <span className="todo-count">
                        <strong>{this.state.model.nIncompleteTodos}</strong>
                        {" "}
                        items left
                    </span>
                    <ul className="filters">
                        <li>
                            <a
                                href="#/"
                                className={
                                    this.state.model.filter == "All" &&
                                        "selected"
                                }
                                onClick={() => {
                                    this.state.model.setFilterType("All");
                                    this.setState({ model: this.state.model });
                                }}
                            >
                                All
                            </a>
                        </li>
                        <li>
                            <a
                                href="#/active"
                                onClick={() => {
                                    this.state.model.setFilterType("Active");
                                    this.setState({ model: this.state.model });
                                }}
                                className={
                                    this.state.model.filter == "Active" &&
                                        "selected"
                                }
                            >
                                Active
                            </a>
                        </li>
                        <li>
                            <a
                                href="#/completed"
                                onClick={() => {
                                    this.state.model.setFilterType("Completed");
                                    this.setState({ model: this.state.model });
                                }}
                                className={
                                    this.state.model.filter == "Completed" &&
                                        "selected"
                                }
                            >
                                Completed
                            </a>
                        </li>
                    </ul>
                    <button
                        onClick={e => {
                            this.state.model.clearCompleted();
                            this.setState({ model: this.state.model });
                        }}
                        className="clear-completed"
                        style={{ display: "block" }}
                    >
                        Clear completed
                    </button>
                </footer>
            </section>
        );
    }
}
