// import React from "react";
// import ReactDOM from "react-dom";
// import App from "./App";
// import assert from "assert";
// import fs from "fs";
// import { promisify } from "util";

// it("renders without crashing", done => {
//     const div = document.createElement("div");
//     ReactDOM.render(<App />, div);
//     document.body.appendChild(div);
//     document.q("button").click();

//     // return Promise.race([promise1, promise2])
//     // setTimeout(function () {
//     //     expect(result).toBe(true);
//     //     done()
//     // }, 1000);

//     // debugger;
//     // expect(div.querySelectorAll(".foo").length).toBe(1);
// });

// describe("es3", () => {
//     it("basit aritmetic", () => {
//         expect(1 + 1).toBe(20);
//     });
//     it("string ve obje", () => {
//         debugger;
//         expect("ahmet" + "ali").toBe(String("ahmetali"));
//         expect(1).toBe(Number("1"));

//         var ad = "isim";
//         expect({ isim: "Ali" }.isim).toBe("Ali");
//         expect({ isim: "Ali" }[ad]).toBe("Ali");
//         expect({ isim: "Ali", 1: "ali" }[1]).toBe("ali");

//         var insan = { isim: "Ali" };

//         expect(insan.soyad).toBe(undefined);

//         insan.soyad = "Ozturk";
//         expect(insan.soyad).toBe("Ozturk");

//         var x = ["1", 2, 3];

//         expect(x[10]).toBe(undefined);
//         expect(x.length).toBe(3);
//         x[10] = 12;

//         expect(x[10]).toBe(12);
//         expect(x.length).toBe(11);
//         expect(x[9]).toBe(undefined);

//         x.push(13);
//         expect(x.length).toBe(12);

//         var y = x.concat([1, 2]);

//         expect(x.length).toBe(12);
//         expect(y.length).toBe(14);

//         y[0] = 127;
//         // expect(x[0]).toBe(127);

//         var isim = "ali";

//         isim + " ozturk";

//         expect(isim).toBe("ali");

//         var c = [{ name: "Ali" }];
//         var d = c.concat([{ name: "Ahmet" }]);

//         expect(c.length).toBe(1);
//         expect(d.length).toBe(2);

//         d[0].name = "Ahmet";
//         expect(c[0].name).toBe("Ahmet");

//         expect(1).toBe(1);
//         expect("1").toBe("1");
//         expect("1").not.toBe(1);

//         expect(1 == "1").toBeTruthy();
//         expect(1 === "1").toBeFalsy();

//         expect("success").not.toBe(true);
//         var x = "";

//         expect("success").toBeTruthy();
//         expect("").not.toBeTruthy();

//         // if (!x) {
//         //     throw new Error("");
//         // }
//         // expect("").toBe(true);
//     });

//     it("should have function scope by default", () => {
//         console.log(x);
//         // console.log(z);
//         expect(x).toBeFalsy();
//         expect(x).not.toBe(3);

//         var x = 3;

//         expect(x).toBe(3);
//     });

//     it("function hoisting", () => {
//         expect(x()).toBe(3);
//         expect(typeof g).not.toBe("function");
//         expect(typeof g).toBe("undefined");
//         expect(function() {
//             g();
//         }).toThrow();

//         function x() {
//             return 3;
//         }

//         var g = function() {
//             return 5;
//         };
//     });

//     it("function scope var dezavantaji", function() {
//         var i;

//         // for (var i = 0; i < 10; i++) {
//         //     // IIFE
//         //     (function(j) {
//         //         document
//         //             .getElementById("foo" + j)
//         //             .addEventHandler("click", function() {
//         //                 console.log(j);
//         //             });
//         //     })(i);
//         // }

//         // console.log(i);

//         expect(i).toBeFalsy();
//     });

//     xit("iki cocuk function", () => {
//         function ana() {
//             var x = 0;

//             function cocuk1() {
//                 var x = 1;
//             }

//             function cocuk2() {
//                 var x = 2;
//             }

//             cocuk1();

//             cocuk2();

//             cocuk1();

//             expect(x).toBe(1);

//             return {
//                 getX: function() {
//                     return x;
//                 },

//                 setX: function(u) {
//                     x = u;
//                 }
//             };
//         }

//         var anne1 = ana();

//         expect(anne1.x).toBeFalsy();

//         expect(anne1.getX()).toBe(1);

//         anne1.setX(5);

//         expect(anne1.getX()).toBe(5);
//     });

//     it("esitlik", () => {
//         expect(1).toBe(1);
//         expect([1, 2]).toEqual([1, 2]);

//         expect([1, 2] == [1, 2]).toBeFalsy();
//         expect([1, 2] === [1, 2]).toBeFalsy();
//     });

//     it("obje keyleri", () => {
//         var x = { 1: "Ali", isim: "Ahmet" };

//         expect(x[1]).toBe("Ali");
//         expect(x["1"]).toBe("Ali");

//         var adres = { sehir: "Ankara" };

//         x[adres] = "Ankara";

//         var telefon = { telefon: "123" };

//         x[telefon] = "345";

//         expect(x.adres).toBeFalsy();
//         expect(x[adres]).toBe("345");

//         console.log(x);
//     });

//     it("this ne demek?", () => {
//         var ali = {
//             ad: "ali",

//             ataBak: function() {
//                 return this.ad + " bakiyor";
//             }
//         };

//         expect(ali.ataBak()).toBe("ali bakiyor");

//         var ataBak = ali.ataBak;

//         expect(function() {
//             ataBak();
//         }).toThrow();

//         function Human(isim, soyisim) {
//             this.isim = isim;
//             this.soyisim = soyisim;

//             this.fullName = function() {
//                 var that = this;

//                 // this = ali
//                 var myFunc = function() {
//                     console.log(that.isim);
//                 };

//                 [(1, 2, 3)].forEach(myFunc);

//                 return this.isim + " " + this.soyisim;
//             };
//         }

//         var ali = new Human("Ali", "Ozturk");

//         expect(ali.isim.length).toBe(3);

//         expect(ali.fullName()).toBe("Ali Ozturk");
//     });

//     it("bind/apply/call", () => {
//         var insan1 = { name: "Ali" };
//         var insan2 = { name: "Burak" };
//         var insan3 = { name: "Cuneyt" };

//         function selamVer() {
//             return this.name;
//         }

//         expect(typeof selamVer.apply).toBe("function");
//         expect(typeof selamVer.call).toBe("function");
//         expect(typeof selamVer.bind).toBe("function");

//         expect(selamVer.apply(insan1)).toBe(insan1.name);
//         expect(selamVer.call(insan1)).toBe("Ali");

//         expect(typeof selamVer.bind(insan1)).toBe("function");
//         expect(() => selamVer()).toThrow();

//         var selamVerInsan1 = selamVer.bind(insan1);

//         expect(selamVerInsan1()).toBe("Ali");
//     });

//     it("fonksiyon parametre sayisi", () => {
//         function deneme(arg1, arg2) {
//             // arguments = 3
//             expect(arguments.length).toBe(3);
//             expect(arguments[0]).toBe(3);
//             expect(typeof arguments.push).not.toBe("function");

//             expect(() => {
//                 arguments.forEach(function(arg) {
//                     console.log(arg);
//                 });
//             }).toThrow();

//             expect(Array.isArray(arguments)).toBeFalsy();

//             console.log("s", Object.getPrototypeOf(arguments));

//             return arg1 + arg2;
//         }

//         expect(Array.isArray([])).toBe(true);
//         expect(Array.isArray("")).toBe(false);
//         // expect(deneme(3)).toBe(NaN);

//         expect(() => deneme(3, 4, 5)).not.toThrow();
//         expect(() => deneme(3, 4, 5, 6)).toThrow();

//         function returnArgs() {
//             var sum = 0;
//             for (var i = 0; i < arguments.length; i++) {
//                 sum += arguments[i];
//             }
//             return sum;
//         }

//         expect(returnArgs.apply(null, [0, 1, 2])).toEqual(3);
//         expect(returnArgs.call(null, 0, 1, 2, 3, 4, 5)).toEqual(15);

//         function returnArgsAsArray() {
//             return Array.from(arguments);
//         }

//         expect(
//             returnArgsAsArray.apply(null, [{ name: "Ali" }, { name: "Ahmet" }])
//         ).toEqual([{ name: "Ali" }, { name: "Ahmet" }]);

//         expect(Array.from({ 0: "Ali", 1: "Ayse", length: 2 })).toEqual([
//             "Ali",
//             "Ayse"
//         ]);
//     });
// });

// describe("assert testleri", () => {
//     it("should use various assert funcs.", () => {
//         assert.ok(1 == 2 - 1);

//         assert(1 == 1, "1 1'e esit olmali");

//         assert.notEqual([1, 2], [1, 2]);
//         assert.deepEqual([1, 2], [1, 2]);

//         // 1.should.equal(2-1);
//     });
// });

// describe("es5 array", () => {
//     it("should do a mapping", () => {
//         var square = function(x) {
//             return x * x;
//         };

//         function cube(x) {
//             return x * x * x;
//         }
//         expect([0, 1, 2].map(cube)).toEqual([0, 1, 8]);

//         var elekFonksiyonu = function(x, i) {
//             return x % 2 == 0 && i < 2;
//         };
//         expect([0, 1, 2].filter(elekFonksiyonu)).toEqual([0]);

//         function map(fn, arr) {
//             var newArray = [];
//             for (var i = 0; i < arr.length; i++) {
//                 newArray.push(fn(arr[i]));
//             }
//             return newArray;
//         }

//         function filter(fn, arr) {
//             var newArray = [];
//             for (var i = 0; i < arr.length; i++) {
//                 if (fn(arr[i], i)) {
//                     newArray.push(arr[i]);
//                 }
//             }
//             return newArray;
//         }

//         function tek(x) {
//             return x % 2 == 1;
//         }
//         expect(map(square, [0, 1, 2])).toEqual([0, 1, 4]);
//         expect(filter(tek, [0, 1, 2])).toEqual([1]);

//         expect(filter(elekFonksiyonu, [0, 1, 2])).toEqual([0]);

//         [0, 1, 2].map(function(item, i, originalArray) {
//             expect(originalArray).toEqual([0, 1, 2]);
//         });

//         [0, 1, 2, 3].filter(function(item, i, originalArray, dorduncu) {
//             expect(originalArray).toEqual([0, 1, 2, 3]);
//             expect(dorduncu).toBeFalsy();
//         });

//         [0, 1, 2].map(square).filter(function(item, i, array) {
//             expect(array).toEqual([0, 1, 4]);
//         });

//         [0, 1, 2].map(x => x * x).filter(x => x % 2 == 0);

//         var array = [0, 1, 2];
//         var bosArray = [];

//         for (var i = 0; i < array.length; i++) {
//             var square = array[i] * array[i];

//             if (square % 2 === 0) {
//                 bosArray.push(square);
//             }
//         }
//         var square2 = x => x * x;
//         var quad = x => square2(square2(x));
//         expect(quad(2)).toBe(16);

//         var topla = (a, b) => a + b;
//         expect(topla(2, 1)).toBe(3);

//         var topla = (a, b) => {
//             var c = a + b;

//             return a + b;
//         };

//         var newPerson = name => ({
//             name,
//             surname: "Ozgur"
//         });
//         expect(newPerson("ustun")).toEqual({ name: "ustun", surname: "Ozgur" });

//         expect(
//             [10, 5, 21].reduce((a, b, c, d) => {
//                 console.log(c, d);
//                 return Math.max(a, b);
//             })
//         ).toBe(21);

//         function myReduce(arr, reducerFn, seed) {
//             var total;

//             if (seed) {
//                 total = reducerFn(seed, arr[0], 0, arr);
//             } else {
//                 total = arr[0];
//             }

//             for (var i = 1; i < arr.length; i++) {
//                 total = reducerFn(total, arr[i], i, arr);
//             }
//             return total;
//         }

//         function reducer(a, b, c, d) {
//             return c;
//         }

//         expect([10, 5, 21].reduce(reducer)).toEqual(
//             myReduce([10, 5, 21], reducer)
//         );

//         expect([10, 5, 21].reduce(topla, 10)).toEqual(
//             myReduce([10, 5, 21], topla, 10)
//         );

//         expect(
//             myReduce([10, 5, 21], (a, b, c, d) => {
//                 return Math.max(a, b);
//             })
//         ).toBe(21);

//         expect([0, 1, 2].reduce((a, b) => a + b, 10)).toBe(13);

//         expect([0, 1, 2].reduce((a, b) => a.concat([b]), [])).toEqual([
//             0,
//             1,
//             2
//         ]);

//         expect([0, 1, 2].reduce((a, b) => a.concat([square2(b)]), [])).toEqual([
//             0,
//             1,
//             4
//         ]);
//     });
// });

// it("should not be constant", () => {
//     const x = 10;

//     expect(() => x = 20).toThrow();

//     const y = { isim: "Ali" };

//     expect(() => {
//         y.isim = "Ahmet";
//     }).not.toThrow();

//     expect(y.isim).toBe("Ahmet");
// });

// it("functions in Es6", () => {
//     function merhaba(ad = "Ustun", selam = "Merhaba") {
//         return selam + " " + ad;
//     }

//     expect(merhaba()).toBe("Merhaba Ustun");

//     expect(merhaba(undefined, "Hello")).toBe("Hello Ustun");

//     function returnAsArray(...args) {
//         return args.map(x => x * 2);
//     }

//     expect(returnAsArray(0, 1, 2)).toEqual([0, 2, 4]);

//     expect(Math.max([0, 10, 5])).not.toBe(10);

//     expect(Math.max.apply(null, [0, 10, 5])).toBe(10);

//     expect(Math.max(...[0, 10, 5])).toBe(10);

//     var x = [2, 3];
//     var y = [4, 5];

//     expect(x.concat([6]).concat(y)).toEqual([2, 3, 6, 4, 5]);
//     expect([...x, 6, ...y]).toEqual([2, 3, 6, 4, 5]);
// });

// it("destructuring", () => {
//     var x = { name: "Ali" };

//     var { name, surname, adres: { sehir } } = {
//         name: "Ali",
//         adres: { sehir: "Ankara" }
//     };
//     expect(surname).toBeFalsy();
//     expect(sehir).toBe("Ankara");
//     expect(name).toBe("Ali");

//     var y = { name };

//     expect(x).toEqual(y);

//     var [a, b] = [1, 2];

//     expect(a).toBe(1);
// });

// it("should create a class", () => {
//     class Human {
//         constructor(name, surname) {
//             this._name = name;
//             this._surname = surname;

//             if (name == "Huseyin") {
//                 this.fullName = function() {
//                     return this.name + " " + this.surname;
//                 };
//             }
//         }

//         get name() {
//             return this._name;
//         }

//         set name(newName) {
//             this._name = newName;
//         }

//         get;

//         doIt() {
//             return this.name.toUpperCase();
//         }

//         static generateHuman() {
//             return new Human("John", "Doe");
//         }
//     }

//     expect(Human.generateHuman().name).toBe("John");

//     var n = new Human("Ustun", "Ozgur");
//     // n.name = "Ahmet";

//     expect(n.name).toBe("Ustun");

//     expect(n.fullName).toBe("Ustun Ozgur");
// });

// it("async/await ornegi", async done => {
//     var readFilePromise = promisify(fs.readFile);

//     var data = await readFilePromise("/etc/hosts", "utf8");

//     expect(data).toBeTruthy();
//     // console.log(data);

//     fs.readFile("/etc/hosts", function(err, data) {
//         // console.log(data);
//         expect(data).toBeTruthy();
//         done();
//     });
// });
