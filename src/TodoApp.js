export class Todo {
    constructor(metin) {
        this.description = metin;
        this.completed = false;
    }

    toggle() {
        this.completed = !this.completed;
    }
}

export class TodoApp {
    constructor() {
        this.geciciTodo = "";
        this.todoListesi = [];
        this.filter = "All";
    }

    setGeciciTodo(metin) {
        this.geciciTodo = metin;
    }

    addNewTodo() {
        var newTodo = new Todo(this.geciciTodo);
        this.todoListesi.push(newTodo);
        this.geciciTodo = "";
        return newTodo;
    }

    getCompletedTodos() {
        return this.todoListesi.filter(todo => todo.completed);
    }

    getIncompleteTodos() {
        return this.todoListesi.filter(todo => !todo.completed);
    }

    get nIncompleteTodos() {
        return this.getIncompleteTodos().length;
    }

    clearCompleted() {
        this.todoListesi = this.getIncompleteTodos();
    }

    getVisibleTodos() {
        if (this.filter == "All") {
            return this.todoListesi;
        } else if (this.filter == "Completed") {
            return this.getCompletedTodos();
        } else {
            return this.getIncompleteTodos();
        }
    }
    toggleTodo(description) {
        this.todoListesi.forEach(function(todo) {
            if (todo.description === description) {
                todo.toggle();
            }
        });
    }

    setFilterType(newFilterType) {
        if (["All", "Completed", "Active"].includes(newFilterType)) {
            this.filter = newFilterType;
        } else {
            throw new Error("Unknown filter type");
        }
    }
}
