import React from "react";
import ReactDOM from "react-dom";
import App from "./TodoAppComponent";
import registerServiceWorker from "./registerServiceWorker";
import "./index.css";

ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();

// function helloWorld() {
//     return "hello"
// }

// window.helloWorld = helloWorld;
