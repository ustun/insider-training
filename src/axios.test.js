import axios from "axios";

test("context", async () => {
    var url = "https://api.github.com/users/ustun/repos";
    try {
        var response = await axios.get(url);
        console.log(response.data);

        expect(response.data.length > 0).toBeTruthy();
    } catch (e) {
        console.log(e);
    }
});
