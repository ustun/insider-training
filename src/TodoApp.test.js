import { TodoApp, Todo } from "./TodoApp";
require("jasmine-check").install();

describe("Maths", () => {
    check.it("addition is commutative", gen.int, gen.int, (numA, numB) => {
        expect(numA + numB).toEqual(numB + numA);
    });
});

describe("Todo Modeli", () => {
    var todo;

    beforeEach(() => {
        todo = new Todo("deneme");
    });

    it("should be able to create a new Todo instance", () => {
        expect(todo.description).toBe("deneme");
        expect(todo.completed).toBeFalsy();
    });

    it("should be able to toggle a todo", () => {
        expect(todo.description).toBe("deneme");
        expect(todo.completed).toBeFalsy();
        todo.toggle();
        expect(todo.completed).toBeTruthy();
        todo.toggle();
        expect(todo.completed).toBeFalsy();
    });
});

describe("TodoApp modeli", () => {
    var todoApp, successful;

    beforeEach(() => {
        todoApp = new TodoApp();
    });

    it("should create a TodoApp instance", () => {
        expect(todoApp.geciciTodo).toBe("");
        expect(todoApp.todoListesi).toEqual([]);
        var successful = true;
    });

    it("should be able to store a temp text", () => {
        todoApp.setGeciciTodo("Utu yap");

        expect(todoApp.geciciTodo).toBe("Utu yap");
    });

    it("should be able to create a new todo from the temp todo", () => {
        todoApp.setGeciciTodo("Utu yap");
        todoApp.addNewTodo();
        expect(todoApp.todoListesi.length).toBe(1);
        expect(todoApp.geciciTodo).toBe("");
    });

    function setAndAdd(todoApp, description) {
        todoApp.setGeciciTodo(description);
        return todoApp.addNewTodo();
    }

    it("should be able to toggle the todo with a description", () => {
        setAndAdd(todoApp, "Utu yap");
        var yemekYapTodo = setAndAdd(todoApp, "Yemek yap");

        todoApp.toggleTodo("Yemek yap");

        expect(yemekYapTodo.completed).toBeTruthy();
    });

    it("should be able to see only completed / incomplete todos", () => {
        setAndAdd(todoApp, "Utu yap");
        setAndAdd(todoApp, "Alisveris yap");
        var yemekYapTodo = setAndAdd(todoApp, "Yemek yap");

        todoApp.toggleTodo("Yemek yap");

        expect(todoApp.getCompletedTodos().length).toBe(1);
        expect(todoApp.getIncompleteTodos().length).toBe(2);

        expect(todoApp.nIncompleteTodos).toBe(2);
    });

    it("should be able to clear completed todos", () => {
        setAndAdd(todoApp, "Utu yap");
        setAndAdd(todoApp, "Alisveris yap");
        var yemekYapTodo = setAndAdd(todoApp, "Yemek yap");

        todoApp.toggleTodo("Yemek yap");

        todoApp.clearCompleted();
    });

    it("should be able to show todos based on the filter", () => {
        setAndAdd(todoApp, "Utu yap");
        setAndAdd(todoApp, "Alisveris yap");
        var yemekYapTodo = setAndAdd(todoApp, "Yemek yap");

        todoApp.toggleTodo("Yemek yap");

        expect(todoApp.getVisibleTodos().length, 3);

        todoApp.setFilterType("Completed");

        expect(todoApp.getVisibleTodos().length, 1);

        todoApp.setFilterType("Active");

        expect(todoApp.getVisibleTodos().length, 2);

        todoApp.setFilterType("All");

        expect(todoApp.getVisibleTodos().length, 3);
    });

    it("should only accept filters All complete and active", () => {
        expect(() => todoApp.setFilterType("armut")).toThrow();
    });
});
